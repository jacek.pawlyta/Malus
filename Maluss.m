% An example how to make a polar plot
% Matlab 2011a
% Author: Jacek Pawlyta
% GPL 2.0 licence
% 2019-10-23

% Create a matrix containing angles in degrees 
phi = 0:5:360;
% start form 0 with step 5 degrees up to 360 degree


% Create a matrix containing distance from the central point
% In the example it is Malus's law I = I0 * (cos(phi))^2
I0 = 14; % I0 is euqal to 14 microamps
I = I0 * cosd(phi).^2;
% Malus's law

% As we have the data, lets plot it on polar plot
polar(phi*pi/180, I, '-b')
% As the plot function requires radians as the parameter we convert degrees
% to radians on the fly and then we plot Mulus's law
% third parameter desribes the line type and colour of the plot 
% Show the plot

saveas(gcf, 'Maluss.pdf');
% Save the plot to pdf file so we can use it in the report

